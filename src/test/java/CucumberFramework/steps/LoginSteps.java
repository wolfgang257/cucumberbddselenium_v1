package CucumberFramework.steps;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.Assert;

import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginSteps {
	WebDriver driver;
	
	@Before() 
	public void setup() throws IOException {
		/**Para chrome*/
		//System.setProperty("webdriver.chrome.driver", Paths.get(System.getProperty("user.dir")).toRealPath() +  "\\src\\test\\java\\CucumberFramework\\resources\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", Paths.get(System.getProperty("user.dir")).toRealPath() +  "/src/test/java/CucumberFramework/resources/lnx/chromedriver");
		this.driver = new ChromeDriver();
		
		/**Para Firefox*/
		/*ystem.setProperty("webdriver.gecko.driver", Paths.get(System.getProperty("user.dir")).toRealPath() +  "/src/test/java/CucumberFramework/resources/lnx/geckodriver");	
		//FirefoxOptions options = new FirefoxOptions(); //solo para firefox viejo
        //options.setCapability("marionette", false);
        this.driver = new FirefoxDriver();
        */   
		this.driver.manage().window().maximize();
		this.driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
		this.driver.manage().timeouts().implicitlyWait(500, TimeUnit.SECONDS);
	}
	
	@After()
	public void down(){
		driver.manage().deleteAllCookies();
		driver.quit();
	}
	

	@Given("^I access webdriverunivsity\\.com$")
	public void i_access_webdriverunivsity_com() throws Throwable {
		driver.get("http://www.webdriveruniversity.com");
		//driver.get("http://www.webdriveruniversity.com/Login-Portal/index.html");
	}

	@When("^I click on the login portal button$")
	public void i_click_on_the_login_portal_button() throws Throwable {
		Thread.sleep(1000);
		driver.findElement(By.id("login-portal")).click();
	}

	@When("^I enter a username$")
	public void i_enter_a_username() throws Throwable {
		@SuppressWarnings("unused")
		String winHandlerBefore = driver.getWindowHandle();
	
		for(String winHandler: driver.getWindowHandles()) {
			driver.switchTo().window(winHandler);
		}
		
		Thread.sleep(4000);
		driver.findElement(By.id("text")).sendKeys("webdriver");
	}

	@When("^I enter a \"([^\"]*)\" password$")
	public void i_enter_a_password(String password) throws Throwable {
		driver.findElement(By.id("password")).sendKeys(password); 
	}

	@When("^I click on the login button$")
	public void i_click_on_the_login_button() throws Throwable {
		driver.findElement(By.id("login-button")).click();

	}

	@Then("^I should be presented with a succesfful validation alert$")
	public void i_should_be_presented_with_a_succesfful_validation_alert() throws Throwable {
		Alert alert = driver.switchTo().alert();
		System.out.println(alert.getText());
		
		Assert.assertEquals(alert.getText(), "validation succeeded");
	}
	
	@Then("^I should be presented with a unsuccesfful validation alert$")
	public void i_should_be_presented_with_a_unsuccesfful_validation_alert() throws Throwable {
		Alert alert = driver.switchTo().alert();
		System.out.println(alert.getText());
		
		Assert.assertEquals(alert.getText(), "validation failed");
	}
	
	/**
	 * 
	 * @throws Throwable
	 */
	@Given("^I access webdriveruniversity$")
	public void i_access_webdriveruniversity() throws Throwable {
		driver.get("http://www.webdriveruniversity.com");
	}

	@When("^I click on the contact us button$")
	public void i_click_on_the_contact_us_button() throws Throwable {
		driver.findElement(By.id("contact-us")).click();
	}

	@And("^I enter a valid first name$")
	public void i_enter_a_valid_first_name() throws Throwable {
		@SuppressWarnings("unused")
		String winHandlerBefore = driver.getWindowHandle();
	
		for(String winHandler: driver.getWindowHandles()) {
			driver.switchTo().window(winHandler);
		}
		
		Thread.sleep(4000);
		driver.findElement(By.cssSelector("input[name='first_name']")).sendKeys("Tom");
	}

	@And("^I enter a valid last name$")
	public void i_enter_a_valid_last_name() throws Throwable {
		driver.findElement(By.cssSelector("input[name='last_name']")).sendKeys("Hanks");
	}

	@And("^I enter a valid email address$")
	public void i_enter_a_valid_email_address() throws Throwable {
		driver.findElement(By.cssSelector("input[name='email']")).sendKeys("boos@mail.cloud");
	}

	@And ("^I enter comments$")
	public void i_enter_comments(DataTable comments) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    // For automatic transformation, change DataTable to one of
	    // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
	    // E,K,V must be a scalar (String, Integer, Date, enum etc)

		List<List<String>> data = comments.raw();
		//System.out.println("I enter comments " + data.get(0).get(0));
		//System.out.println("I enter comments " + data.get(0).get(1));
		driver.findElement(By.cssSelector("textarea[name='message']")).sendKeys(data.get(1).get(0)+"\n");//c1
		driver.findElement(By.cssSelector("textarea[name='message']")).sendKeys(data.get(1).get(1)); //c2
	}

	@When("^I click on the submit button$")
	public void i_click_on_the_submit_button() throws Throwable {
		driver.findElement(By.cssSelector("input[value='SUBMIT']")).click();
	}

	@Then("^the information should successfully be submitted via the contact us form$")
	public void the_information_should_successfully_be_submitted_via_the_contact_us_form() throws Throwable {
	    //algo
	}



}
