Feature: Login to account at webdriverunivsity.com using login portal 

Background: 
	Given I access webdriverunivsity.com
	When I click on the login portal button
	And I enter a username

Scenario: Login account with a valid password
	And I enter a "webdriver123" password
	When I click on the login button
	Then I should be presented with a succesfful validation alert

Scenario: Login account with a invalid password
	And I enter a "invalid" password
	When I click on the login button
	Then I should be presented with a unsuccesfful validation alert  
	
Scenario: Submit valid data via contact us form
	Given I access webdriveruniversity
	When I click on the contact us button
	And I enter a valid first name
	And I enter a valid last name
	And I enter a valid email address
	And I enter comments
	| example comment one | example comment two |
	| example comment one r | example comment two r |
	When I click on the submit button
	Then the information should successfully be submitted via the contact us form	